﻿/// Arthur M. Thomé
/// 28 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class Pot : MonoBehaviour
{
    #region Fields

    #region Constants

    private const string C_ANIMATION_PARAMETER_Break = "Break";

    #endregion

    [ SerializeField ] private Animator m_animatorPot = null;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        
    }
    
    void Update ( )
    {
        
    }

    #endregion

    public void BreakPot ( )
    {
        m_animatorPot.SetBool ( C_ANIMATION_PARAMETER_Break, true );

        StartCoroutine ( TimeToHide ( ) );
    }

    private IEnumerator TimeToHide ( )
    {
        yield return new WaitForSeconds ( 0.3f );

        this.gameObject.SetActive ( false );
    }
}