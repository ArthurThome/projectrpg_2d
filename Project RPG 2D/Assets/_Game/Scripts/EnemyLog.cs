﻿/// Arthur M. Thomé
/// 28 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class EnemyLog : Enemy
{
    #region Fields

    #region Constants

    private const string C_TAG_PLAYER = "Player";

    #endregion

    [SerializeField ] private Transform m_target = null;
    [ SerializeField ] private Transform m_homePosition = null;

    [ SerializeField ] private float m_chaseRadius = 0;
    [ SerializeField ] private float m_attackRadius = 0;

    #endregion

    //melhoras sistema de target!
    //criar widgets quando spawnar enemys.

        //talvez tirar do update e fazer um raycast

    #region MonoBehavior Methods

    void Start ( )
    {
        m_target = GameObject.FindGameObjectWithTag ( C_TAG_PLAYER ).transform;
    }
    
    void Update ( )
    {
        CheckDistance ( );
    }

    #endregion

    private void CheckDistance ( )
    {
        if ( Vector3.Distance ( m_target.position, transform.position ) <= m_chaseRadius 
                && Vector3.Distance ( m_target.position, transform.position ) > m_attackRadius )
        {
            transform.position = Vector3.MoveTowards ( transform.position, m_target.position, m_moveSpeed * Time.deltaTime );
        }
    }
}