﻿/// Arthur M. Thomé
/// 27 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using TMPro;
using UnityEngine;


#endregion

public class PersistentCanvas : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private TextMeshProUGUI m_mainRoomTitle;
    [ SerializeField ] public RectTransform m_titleRoomPanel;
    [ SerializeField ] private bool m_needText;

    [ SerializeField ] private TextMeshProUGUI m_txtDialogue = null;
    public RectTransform m_dialoguePanel;
    public RectTransform m_interactPanel;

    #endregion

    #region MonoBehavior Methods

    void Awake ( )
    {
        HidePanel ( m_titleRoomPanel );
        HidePanel ( m_dialoguePanel );
        HidePanel ( m_interactPanel );
    }
    
    void Update ( )
    {
        
    }

    #endregion

    public void StartCoroutineNameRoom ( string room )
    {
        StartCoroutine ( PlaceNameRoom ( room ) );
    }

    private IEnumerator PlaceNameRoom ( string room )
    {
        ShowPanel ( m_titleRoomPanel );
        m_mainRoomTitle.text = room;
        yield return new WaitForSeconds ( 3f );

        HidePanel ( m_titleRoomPanel );
    }

    public void HidePanel ( RectTransform panel )
    {
        panel.gameObject.SetActive ( false );
    }

    public void ShowPanel ( RectTransform panel )
    {
        panel.gameObject.SetActive ( true );
    }

    public void OpenDialogueSign ( string text )
    {
        if ( m_dialoguePanel.gameObject.activeInHierarchy )
            {
                HidePanel ( m_dialoguePanel );
            }
            else
            {
                ShowPanel ( m_dialoguePanel );
                m_txtDialogue.text = text;
            }
    }
}