﻿/// Arthur M. Thomé
/// 28 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

//Melhoras sistema de collider, attack Down as vezes nao pega o collider.

public class PlayerHit : MonoBehaviour
{
    #region Fields

    #region Constants

    private const string C_TAG_BREAKABLE = "Breakable";

    #endregion

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        
    }
    
    void Update ( )
    {
        
    }

    #endregion

    public void OnTriggerEnter2D ( Collider2D other )
    {
        if ( other.CompareTag ( C_TAG_BREAKABLE ) )
        {
            other.GetComponent < Pot > ( ).BreakPot ( );
        }
    }
}