﻿/// Arthur M. Thomé
/// 27 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

#endregion

public class Sign : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private PersistentCanvas m_instancePersistentCanvas = null;

    [ SerializeField ] private string m_dialogue = "";
    [ SerializeField ] private bool m_playerInRange = false;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        
    }    
    
    void Update ( )
    {
        if ( Input.GetKeyDown ( KeyCode.E ) && m_playerInRange )
        {
            m_instancePersistentCanvas.OpenDialogueSign ( m_dialogue );
            m_instancePersistentCanvas.HidePanel ( m_instancePersistentCanvas.m_interactPanel );
        }
    } 

    #endregion

    private void OnTriggerEnter2D ( Collider2D other )
    {
        if ( other.CompareTag ( "Player" ) )
        {
            m_playerInRange = true;
            m_instancePersistentCanvas.ShowPanel ( m_instancePersistentCanvas.m_interactPanel );
        }
    }

    private void OnTriggerExit2D ( Collider2D other )
    {
        if ( other.CompareTag ( "Player" ) )
        {
            m_playerInRange = false;
            m_instancePersistentCanvas.HidePanel ( m_instancePersistentCanvas.m_dialoguePanel );
            m_instancePersistentCanvas.HidePanel ( m_instancePersistentCanvas.m_interactPanel );
        }
    }
}