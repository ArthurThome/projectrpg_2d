﻿/// Arthur M. Thomé
/// 28 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class Enemy : MonoBehaviour
{
    #region Fields

    [ SerializeField ] protected float m_health = 0;
    [ SerializeField ] protected string m_enemyName = "";
    [ SerializeField ] protected float m_baseAttack = 0;

    [ SerializeField ] protected float m_moveSpeed = 0;


    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        
    }
    
    void Update ( )
    {
        
    }

    #endregion
}