﻿/// Arthur M. Thomé
/// 27 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion


public enum PlayerState
{
    ATTACK,
    WALK,
    INTERACT
}

public class PlayerMovement : MonoBehaviour
{
    #region Fields

    #region Constants

    private const string C_ANIMATION_PARAMETER_MOV_X = "MovX";
    private const string C_ANIMATION_PARAMETER_MOV_Y = "MovY";
    private const string C_ANIMATION_PARAMETER_MOVING = "Moving";
    private const string C_ANIMATION_PARAMETER_ATTACKING = "Attacking";

    #endregion

    [SerializeField ] private float m_playerSpeed = 3.5f;
    [ SerializeField ] private Rigidbody2D m_playerRigidbody = null;
    [ SerializeField ] private Vector3 m_playerVelocity = Vector2.zero;

    [ SerializeField ] private Animator m_playerAnimator = null;

    private PlayerState m_currentState = PlayerState.WALK;

    #endregion

    //decidir se o player vai andar na diagonal ou apenas na vertical e horizontal. 

    #region MonoBehavior Methods

    void Start ( )
    {
        m_playerSpeed = 5f;
    }
    
    void Update ( )
    {
        Debug.Log(m_playerSpeed);
        if ( m_playerSpeed != 5 ) m_playerSpeed = 5f;

        m_playerVelocity = Vector3.zero;

        //esses if sao para nao andar na diagonal, apenas vertical e horizontal
        if ( m_playerVelocity.y == 0 ) m_playerVelocity.x = Input.GetAxisRaw ( "Horizontal" );
        if ( m_playerVelocity.x == 0 ) m_playerVelocity.y = Input.GetAxisRaw ( "Vertical" );

        if ( Input.GetKeyDown ( KeyCode.Mouse0 ) && m_currentState != PlayerState.ATTACK )
        {
            StartCoroutine ( AttackPlayer ( ) );
        }
        else if ( m_currentState == PlayerState.WALK )
        {
            UpdateAnimationAndMove ( );
        }
    }

    #endregion

    private void UpdateAnimationAndMove ( )
    {
        if ( m_playerVelocity != Vector3.zero )
        {
            MoveCharacter ( );
            m_playerAnimator.SetFloat ( C_ANIMATION_PARAMETER_MOV_X, m_playerVelocity.x );
            m_playerAnimator.SetFloat ( C_ANIMATION_PARAMETER_MOV_Y, m_playerVelocity.y );
            m_playerAnimator.SetBool ( C_ANIMATION_PARAMETER_MOVING, true );
        }
        else
        {
            m_playerAnimator.SetBool ( C_ANIMATION_PARAMETER_MOVING, false );
        }
    }

    private void MoveCharacter ( )
    {
        //andar na diagonal sem acelerar o personagem
        //m_playerVelocity.Normalize ( );

        m_playerRigidbody.MovePosition ( transform.position + m_playerVelocity * m_playerSpeed * Time.deltaTime );
    }

    private IEnumerator AttackPlayer ( )
    {
        m_playerAnimator.SetBool ( C_ANIMATION_PARAMETER_ATTACKING, true );
        m_currentState = PlayerState.ATTACK;

        yield return null; // wait 1 frame

        m_playerAnimator.SetBool ( C_ANIMATION_PARAMETER_ATTACKING, false );

        yield return new WaitForSeconds ( 0.33f );

        m_currentState = PlayerState.WALK;
    }
}