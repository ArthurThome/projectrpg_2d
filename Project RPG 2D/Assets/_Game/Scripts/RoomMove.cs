﻿/// Arthur M. Thomé
/// 27 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

#endregion

public class RoomMove : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private Vector2 m_cameraChange;
    [ SerializeField ] private Vector3 m_playerChange;

    [ SerializeField ] private string m_nameRoom = "";

    [ SerializeField ] private CameraMovement m_cam;

    [ SerializeField ] private PersistentCanvas m_instancePersistentCanvas = null;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        
    }
    
    void Update ( )
    {
        
    }

    #endregion

    private void OnTriggerEnter2D ( Collider2D other )
    {
        if ( other.CompareTag ( "Player" ) )
        {

            m_cam.AddMaxPosition ( m_cameraChange );
            m_cam.AddMinPosition ( m_cameraChange );

            other.transform.position += m_playerChange;

            m_instancePersistentCanvas.StartCoroutineNameRoom ( m_nameRoom );
        }
    }
}