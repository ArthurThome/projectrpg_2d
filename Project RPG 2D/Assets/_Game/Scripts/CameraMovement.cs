﻿/// Arthur M. Thomé
/// 27 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class CameraMovement : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private Transform m_target = null;
    [ SerializeField ] private float m_smoothing = 0f;

    [ SerializeField ] private Vector2 m_maxPosition;
    [ SerializeField ] private Vector2 m_minPosition;

    #endregion

    #region MonoBehavior Methods

    private void Start ( )
    {
        
    }
    
    private void LateUpdate ( )
    {
        if ( transform.position != m_target.position )
        {
            Vector3 targetPosition = new Vector3 ( m_target.position.x, m_target.position.y, transform.position.z );

            targetPosition.x = Mathf.Clamp ( targetPosition.x, m_minPosition.x, m_maxPosition.x );
            targetPosition.y = Mathf.Clamp ( targetPosition.y, m_minPosition.y, m_maxPosition.y );


            transform.position = Vector3.Lerp ( transform.position, targetPosition, m_smoothing );
        }
    }

    #endregion

    public void AddMaxPosition ( Vector2 vec )
    {
        m_maxPosition += vec;
    }

    public void AddMinPosition ( Vector2 vec )
    {
        m_minPosition += vec;
    }
}